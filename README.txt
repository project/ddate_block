DESCRIPTION
===========
Provides configurable blocks displaying a Discordian date.

INSTALLATION
============
1. Install as usual, see https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure for further information.
2. The module has no special configuration. All settings are available in the
   block settings:
   /admin/structure/block

MAINTAINER
==========
- dweichert (David Weichert)
